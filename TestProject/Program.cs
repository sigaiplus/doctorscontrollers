using System.Text.Json;
using System.Text.Json.Serialization;
using TestProject.DAL;
using TestProject.Filters;
using TestProject.Services;
using TestProject.Services.Interfaces;
using TestProject.ViewModels;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers(op =>
        op.Filters.Add(new HttpResponseExceptionFilter()))
    .AddJsonOptions(op =>
    {
        op.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
        op.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
    });

builder.Services.AddTransient<HospitalContext>();
builder.Services.AddTransient<IPatientsService, PatientsService>();
builder.Services.AddTransient<IDoctorsService, DoctorsService>();
builder.Services.AddTransient<IPageLoaderService, PageLoaderService>();
builder.Services.AddTransient<IViewModelConverter<Doctor, DoctorViewModel>, DoctorViewModelConverter>();
builder.Services.AddTransient<IViewModelConverter<Patient, PatientViewModel>, PatientViewModelConverter>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//замапить контроллеры
app.MapControllers();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();