﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestProject.DAL;
using TestProject.PageLoad;
using TestProject.Services.Interfaces;
using TestProject.ViewModels;

namespace TestProject.Controllers;

/// <summary>
/// Контроллер врачей
/// </summary>
[Produces("application/json")]
[Route("api/[controller]")]
[ApiController]
public class DoctorsController : ControllerBase
{
    private readonly HospitalContext _hospitalDataContext;
    private readonly IDoctorsService _doctorsService;

    /// <summary>
    /// Добавление записи врача
    /// </summary>
    /// <param name="doctor"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task Create(Doctor doctor, CancellationToken token)
    {
        await _doctorsService.Add(doctor, token);
    }
    
    /// <summary>
    /// Получение записи врача
    /// </summary>
    /// <param name="id"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("{id:int}")]
    public async Task<Doctor> Get(int id, CancellationToken token)
    {
        return await _doctorsService.Get(id, token);
    }

    /// <summary>
    /// Редактирование записи врача
    /// </summary>
    /// <param name="doctor"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task Put(Doctor doctor, CancellationToken token)
    {
        await _doctorsService.Edit(doctor, token);
    }

    /// <summary>
    /// Получение списка врачей
    /// </summary>
    /// <param name="options"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<IEnumerable<DoctorViewModel>> GetPage(PageOptions options, CancellationToken token)
    {
        return await _doctorsService.GetPage(options, token);
    }

    /// <summary>
    /// Удаление записи врача
    /// </summary>
    /// <param name="id"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpDelete]
    public async Task Delete(int id, CancellationToken token)
    {
        await _doctorsService.Delete(id, token);
    }
}