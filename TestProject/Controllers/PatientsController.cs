﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestProject.DAL;
using TestProject.PageLoad;
using TestProject.Services.Interfaces;
using TestProject.ViewModels;

namespace TestProject.Controllers;

/// <summary>
/// Контроллер пациентов
/// </summary>
[Produces("application/json")]
[Route("api/[controller]")]
[ApiController]
public class PatientsController : ControllerBase
{
    private readonly HospitalContext _hospitalDataContext;
    private readonly IPatientsService _patientsService;

    public PatientsController(HospitalContext hospitalDataContext,
        IPatientsService patientsService)
    {
        _hospitalDataContext = hospitalDataContext;
        _patientsService = patientsService;
    }
    
    /// <summary>
    /// Добавление записи пациента
    /// </summary>
    /// <param name="patient"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task Create(Patient patient,CancellationToken token)
    {
        await _patientsService.Add(patient, token);
    }

    /// <summary>
    /// Получение записи пациента
    /// </summary>
    /// <param name="id"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("{id:int}")]
    public async Task<Patient> Get(int id, CancellationToken token)
    {
        return await _patientsService.Get(id, token);
    }

    /// <summary>
    /// Редактирование записи пациента
    /// </summary>
    /// <param name="patient"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task Put(Patient patient, CancellationToken token)
    {
        await _patientsService.Edit(patient, token);
    }

    /// <summary>
    /// Получение списка пациентов
    /// </summary>
    /// <param name="options"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<IEnumerable<PatientViewModel>> GetPage(PageOptions options, CancellationToken token)
    {
        return await _patientsService.GetPage(options, token);
    }

    /// <summary>
    /// Удаление записи пациента
    /// </summary>
    /// <param name="id"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpDelete]
    public async Task Delete(int id, CancellationToken token)
    {
        await _patientsService.Delete(id, token);
    }
}