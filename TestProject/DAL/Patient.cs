﻿namespace TestProject.DAL;

public partial class Patient
{
    public int Id { get; set; }
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string MidleName { get; set; } = null!;
    public string Address { get; set; } = null!;
    public DateTime BirthDate { get; set; }
    public string Sex { get; set; } = null!;
    public int DistrictId { get; set; }

    public virtual District District { get; set; } = null!;
}