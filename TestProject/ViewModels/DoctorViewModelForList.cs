﻿namespace TestProject.ViewModels;

public class DoctorViewModelForList
{
    public string Fio { get; set; } = null!;
    public int Cabinet { get; set; }
    public string Specialization { get; set; }
    public int? District { get; set; }
}