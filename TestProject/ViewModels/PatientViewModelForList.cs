﻿namespace TestProject.ViewModels;

public class PatientViewModelForList
{
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string MidleName { get; set; } = null!;
    public string Address { get; set; } = null!;
    public DateTime BirthDate { get; set; }
    public string Sex { get; set; } = null!;
    public int District { get; set; }
}