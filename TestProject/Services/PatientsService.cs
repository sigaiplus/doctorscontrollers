﻿using System.Net;
using Microsoft.EntityFrameworkCore;
using TestProject.DAL;
using TestProject.Filters;
using TestProject.PageLoad;
using TestProject.Services.Interfaces;
using TestProject.ViewModels;

namespace TestProject.Services;

public class PatientsService : IPatientsService
{
    private readonly HospitalContext _dataContext;
    private readonly IViewModelConverter<Patient, PatientViewModel> _converter;

    public PatientsService(HospitalContext dataContext, IViewModelConverter<Patient, PatientViewModel> converter)
    {
        _dataContext = dataContext;
        _converter = converter;
    }
    
    public async Task Add(Patient patient, CancellationToken token)
    {
        try
        {
            await _dataContext.AddAsync(patient, token);
            await _dataContext.SaveChangesAsync(token);
        }
        catch (Exception ex)
        {
            throw new HttpResponseException(HttpStatusCode.BadRequest, ex.Message);
        }
    }

    public async Task Edit(Patient patient, CancellationToken token)
    {
        var patientBase = await _dataContext.Patients.FirstOrDefaultAsync(a => a.Id == patient.Id, token);
        if (patientBase == null) throw new HttpResponseException(HttpStatusCode.BadRequest, "No such patient");

        patientBase.FirstName = patient.FirstName;
        patientBase.LastName = patient.LastName;
        patientBase.MidleName = patient.MidleName;
        patientBase.Address = patient.Address;
        patientBase.BirthDate = patient.BirthDate;
        patientBase.Sex = patient.Sex;
        patientBase.DistrictId = patient.DistrictId;

        try
        {
            await _dataContext.SaveChangesAsync(token);
        }
        catch(Exception ex)
        {
            throw new HttpResponseException(HttpStatusCode.BadRequest, ex.Message);
        }
    }

    public async Task<Patient> Get(int id, CancellationToken token)
    {
        var patient = await _dataContext.Patients.FirstOrDefaultAsync(a => a.Id == id, token);
        if (patient == null) throw new HttpResponseException(HttpStatusCode.BadRequest, "No such patient");
        return patient;
    }

    public async Task<IEnumerable<PatientViewModel>> GetPage(PageOptions options, CancellationToken token)
    {
        var patients = await _dataContext.Patients.Include(p => p.District).ToListAsync(cancellationToken: token);
        
        //sort

        var result = patients.Select(p => _converter.ToViewModel(p));

        return result.Skip(options.PageNumber*options.PageSize).Take(options.PageSize);
    }

    public async Task<int> Delete(int id, CancellationToken token)
    {
        var patient = await _dataContext.Patients.FirstOrDefaultAsync(p => p.Id == id, token);
        if (patient == null) throw new HttpResponseException(HttpStatusCode.BadRequest, "No such patient");
        var entriesTouched = 0;
        try
        {
            _dataContext.Remove(patient);
            entriesTouched = await _dataContext.SaveChangesAsync(token);
        }
        catch (Exception ex)
        {
            throw new HttpResponseException(HttpStatusCode.BadRequest, ex.Message);
        }

        return entriesTouched;
    }
}