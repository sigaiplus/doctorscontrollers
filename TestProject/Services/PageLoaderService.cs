﻿using TestProject.PageLoad;
using TestProject.Services.Interfaces;

namespace TestProject.Services;

public class PageLoaderService : IPageLoaderService
{
    public IEnumerable<T> LoadPage<T>(IEnumerable<T> entity, PageOptions options) where T : class
    {
        if (options.FieldSort.FieldName == null) return entity;

        var property = entity.First().GetType().GetProperty(options.FieldSort.FieldName);
        if (property != null)
            entity = options.FieldSort.SortType == SortType.Ascending
                ? entity.OrderBy(g => g.GetType().GetProperty(property.Name))
                : entity.OrderByDescending(g => g.GetType().GetProperty(property.Name));


        return entity.Skip(options.PageNumber * options.PageSize).Take(options.PageSize);
    }
}