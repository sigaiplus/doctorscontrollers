﻿using TestProject.DAL;
using TestProject.Services.Interfaces;
using TestProject.ViewModels;

namespace TestProject.Services;

public class PatientViewModelConverter:IViewModelConverter<Patient,PatientViewModel>
{
    public Patient ToModel(PatientViewModel viewModel)
    {
        throw new NotImplementedException();
    }

    public PatientViewModel ToViewModel(Patient model)
    {
        var patient = new PatientViewModel
        {
            FirstName = model.FirstName,
            LastName = model.LastName,
            MidleName = model.MidleName,
            Address = model.Address,
            BirthDate = model.BirthDate,
            Sex = model.Sex,
            District = model.District.Number
        };
        return patient;
    }
}