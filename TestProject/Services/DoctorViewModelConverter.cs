﻿using TestProject.DAL;
using TestProject.Services.Interfaces;
using TestProject.ViewModels;

namespace TestProject.Services;

public class DoctorViewModelConverter:IViewModelConverter<Doctor,DoctorViewModel>
{
    public Doctor ToModel(DoctorViewModel viewModel)
    {
        throw new NotImplementedException();
    }

    public DoctorViewModel ToViewModel(Doctor model)
    {
        var doctorViewModel = new DoctorViewModel();
        doctorViewModel.Fio = model.Fio;
        doctorViewModel.Cabinet = model.Cabinet.Number;
        doctorViewModel.Specialization = model.Specialization.Name;
        if(model.District!=null) doctorViewModel.District = model.District.Number;

        return doctorViewModel;

    }
}