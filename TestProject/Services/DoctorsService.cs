﻿using System.Net;
using Microsoft.EntityFrameworkCore;
using TestProject.DAL;
using TestProject.Filters;
using TestProject.PageLoad;
using TestProject.Services.Interfaces;
using TestProject.ViewModels;

namespace TestProject.Services;

public class DoctorsService : IDoctorsService
{
    private readonly HospitalContext _dataContext;
    private readonly IViewModelConverter<Doctor,DoctorViewModel> _converter;

    public DoctorsService(HospitalContext dataContext, IViewModelConverter<Doctor, DoctorViewModel> converter)
    {
        _dataContext = dataContext;
        _converter = converter;
    }

    public async Task Add(Doctor doctor, CancellationToken token)
    {
        try
        {
            await _dataContext.AddAsync(doctor, token);
            await _dataContext.SaveChangesAsync(token);
        }
        catch (Exception ex)
        {
            throw new HttpResponseException(HttpStatusCode.BadRequest, ex.Message);
        }
    }

    public async Task Edit(Doctor doctor, CancellationToken token)
    {
        var doctorBase = await _dataContext.Doctors.FirstOrDefaultAsync(a => a.Id == doctor.Id, token);
        if (doctorBase == null) throw new HttpResponseException(HttpStatusCode.BadRequest, "No such doctor");

        doctorBase.Fio = doctor.Fio;
        doctorBase.CabinetId = doctor.CabinetId;
        doctorBase.SpecializationId = doctor.SpecializationId;
        if(doctor.DistrictId!=null) doctorBase.DistrictId = doctor.DistrictId;

        try
        {
            await _dataContext.SaveChangesAsync(token);
        }
        catch (Exception ex)
        {
            throw new HttpResponseException(HttpStatusCode.BadRequest, ex.Message);
        }
    }

    public async Task<Doctor> Get(int id, CancellationToken token)
    {
        var doctor = await _dataContext.Doctors.FirstOrDefaultAsync(a => a.Id == id,token);
        if (doctor == null) throw new HttpResponseException(HttpStatusCode.BadRequest, "Doctor who?");
        return doctor;
    }

    public async Task<IEnumerable<DoctorViewModel>> GetPage(PageOptions options, CancellationToken token)
    {
        var doctor = await _dataContext.Doctors.Include(d => d.Cabinet)
            .Include(d => d.Specialization)
            .Include(d => d.District).ToListAsync(token);

        var result = doctor.Select(d => _converter.ToViewModel(d));
        //sort
        return result.Skip(options.PageNumber * options.PageSize).Take(options.PageSize);
    }

    public async Task<int> Delete(int id, CancellationToken token)
    {
        var doctor = await _dataContext.Doctors.FirstOrDefaultAsync(d => d.Id == id,token);
        if (doctor == null) throw new HttpResponseException(HttpStatusCode.BadRequest, "No such doctor");
        var entriesTouched = 0;
        try
        {
            _dataContext.Remove(doctor);
            entriesTouched = await _dataContext.SaveChangesAsync(token);
        }
        catch (Exception ex)
        {
            throw new HttpResponseException(HttpStatusCode.BadRequest, ex.Message);
        }

        return entriesTouched;
    }
}