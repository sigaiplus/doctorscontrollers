﻿using TestProject.DAL;
using TestProject.PageLoad;
using TestProject.ViewModels;

namespace TestProject.Services.Interfaces;

public interface IPatientsService
{
    public Task Add(Patient patient, CancellationToken token);
    public Task Edit(Patient patient, CancellationToken token);
    public Task<Patient> Get(int id, CancellationToken token);
    public Task<IEnumerable<PatientViewModel>> GetPage(PageOptions options, CancellationToken token);
    public Task<int> Delete(int id, CancellationToken token);
}