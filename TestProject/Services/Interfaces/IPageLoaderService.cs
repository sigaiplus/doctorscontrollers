﻿using TestProject.PageLoad;

namespace TestProject.Services.Interfaces;

public interface IPageLoaderService
{
    public IEnumerable<T> LoadPage<T>(IEnumerable<T> entity, PageOptions options) where T : class;
}