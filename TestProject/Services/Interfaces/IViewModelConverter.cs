﻿namespace TestProject.Services.Interfaces;


public interface IViewModelConverter<TModel, TViewModel>
{
    TModel ToModel(TViewModel viewModel);
    TViewModel ToViewModel(TModel model);
}