﻿using TestProject.DAL;
using TestProject.PageLoad;
using TestProject.ViewModels;

namespace TestProject.Services.Interfaces;

public interface IDoctorsService
{
    public Task Add(Doctor doctor, CancellationToken token);
    public Task Edit(Doctor doctor, CancellationToken token);
    public Task<Doctor> Get(int id, CancellationToken token);
    public Task<IEnumerable<DoctorViewModel>> GetPage(PageOptions options, CancellationToken token);
    public Task<int> Delete(int id, CancellationToken token);
}