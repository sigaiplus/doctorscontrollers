﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace TestProject.Filters;

public class HttpResponseExceptionFilter : IActionFilter, IOrderedFilter
{
    /// <summary>
    /// 
    /// </summary>
    public int Order { get; set; } = int.MaxValue - 10;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    public void OnActionExecuting(ActionExecutingContext context) { }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (!(context.Exception is HttpResponseException exception)) return;

        context.Result = new ObjectResult(exception.Value)
        {
            StatusCode = (int)exception.Status
        };
        context.ExceptionHandled = true;
    }
}