﻿namespace TestProject.PageLoad;

public class PageOptions
{
    /// <summary>
    /// Номер страницы
    /// </summary>
    public int PageNumber { get; set; }

    /// <summary>
    /// Номер страницы
    /// </summary>
    public int PageSize { get; set; }

    /// <summary>
    /// Поле сортировки 
    /// </summary>
    public FieldSort FieldSort { get; set; }
    
    public PageOptions()
    {
    }

    public PageOptions(int pageNumber, int pageSize)
    {
        PageNumber = pageNumber;
        PageSize = pageSize;
    }
}